package com.tikan.primalocationmanager

import android.app.Activity
import android.content.Context
import android.location.LocationManager
import android.util.Log
import android.widget.Toast
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationSettingsStatusCodes
import com.google.android.gms.location.SettingsClient

const val GPS_CODE = 101

class GPSUtils(context: Context) {

    private val TAG = "GPS"
    private val mContext: Context = context
    private var mSettingsClient: SettingsClient? = null
    private var mLocationSettingsRequest: LocationSettingsRequest? = null
    private var mLocationManager: LocationManager? = null
    private var mLocationRequest: com.google.android.gms.location.LocationRequest? = null

    init {

        mLocationManager = mContext.getSystemService(Context.LOCATION_SERVICE) as? LocationManager

        mSettingsClient = LocationServices.getSettingsClient(mContext)

        mLocationRequest = com.google.android.gms.location.LocationRequest.create()
        mLocationRequest?.priority =
            com.google.android.gms.location.LocationRequest.PRIORITY_HIGH_ACCURACY

        mLocationRequest?.interval = 1000
        mLocationRequest?.fastestInterval = 500

        if (mLocationRequest != null) {
            val builder: LocationSettingsRequest.Builder = LocationSettingsRequest.Builder()
            builder.addLocationRequest(mLocationRequest!!)

            mLocationSettingsRequest = builder.build()
        }
    }

    fun turnOnGPS() {
        if (mLocationManager?.isProviderEnabled(LocationManager.GPS_PROVIDER) == false) {
            mSettingsClient?.checkLocationSettings(mLocationSettingsRequest!!)
                ?.addOnSuccessListener {
                    Log.d(TAG, "turnOnGPS: Already enable")
                }
                ?.addOnFailureListener { ex ->
                    if ((ex as ApiException).statusCode == LocationSettingsStatusCodes.RESOLUTION_REQUIRED) {

                        try {
                            val resolvableApiException = ex as ResolvableApiException

                            resolvableApiException.startResolutionForResult(
                                mContext as Activity,
                                GPS_CODE
                            )
                        } catch (e: Exception) {
                            Log.d(TAG, "turnOnGPS: unable to start default functionality of GPS")
                        }
                    } else {
                        if ((ex as ApiException).statusCode == LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE) {

                            val errorMessage =
                                "Location Settings are inadequate, and cannot be " + "fixed here, Fix in Settings."

                            Log.e(TAG, errorMessage)
                            Toast.makeText(mContext, errorMessage, Toast.LENGTH_SHORT).show()
                        }
                    }
                }
        }
    }
}