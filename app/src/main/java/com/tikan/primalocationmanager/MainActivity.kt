package com.tikan.primalocationmanager

import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import com.tikan.primalocationmanager.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    var TAG: String = "Location_ankit"
    lateinit var checkPermission: CheckPermission
    lateinit var locationManager: PrimaLocationManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        checkPermission = CheckPermission(this)
        locationManager = PrimaLocationManager(this)



        //get latitude
        binding.btnLatitude.setOnClickListener {
            if (checkPermission.checkPermission()) {
                locationManager.getLatitude {
                    val intent = Intent(this, ShowLocationActivity::class.java)
                    intent.putExtra("location", "$it")
                    startActivity(intent)
                }

            } else {
                checkPermission.requestPermission()
            }
        }

        binding.btnLongitude.setOnClickListener {

            if (checkPermission.checkPermission()) {
                locationManager.getLongitude {
                    val intent = Intent(this, ShowLocationActivity::class.java)
                    intent.putExtra("location", "$it")
                    startActivity(intent)
                }
            } else {
                checkPermission.requestPermission()
            }
        }

        //get location
        binding.btnGetLocation.setOnClickListener {
            if (checkPermission.checkPermission()) {
                locationManager.getLocation {
                    val intent = Intent(this, ShowLocationActivity::class.java)
                    intent.putExtra("location", it)
                    startActivity(intent)
                }
            } else {
                checkPermission.requestPermission()
            }
        }

        //get country name
        binding.btnGetCountryName.setOnClickListener {
            if (checkPermission.checkPermission()) {
                locationManager.getCountryName {
                    val intent = Intent(this, ShowLocationActivity::class.java)
                    intent.putExtra("location", it)
                    startActivity(intent)
                }
            } else {
                checkPermission.requestPermission()
            }
        }

        //get state name
        binding.btnStateName.setOnClickListener {
            if (checkPermission.checkPermission()) {
                locationManager.getStateName {
                    val intent = Intent(this, ShowLocationActivity::class.java)
                    intent.putExtra("location", it)
                    startActivity(intent)
                }
            } else {
                checkPermission.requestPermission()
            }
        }

        //get country code
        binding.btnCountryCode.setOnClickListener {
            if (checkPermission.checkPermission()) {
                locationManager.getCountryCode {
                    val intent = Intent(this, ShowLocationActivity::class.java)
                    intent.putExtra("location", it)
                    startActivity(intent)
                }
            } else {
                checkPermission.requestPermission()
            }
        }

        //get city name
        binding.btnGetCityName.setOnClickListener {
            if (checkPermission.checkPermission()) {
                locationManager.getCityName {
                    val intent = Intent(this, ShowLocationActivity::class.java)
                    intent.putExtra("location", it)
                    startActivity(intent)
                }
            } else {
                checkPermission.requestPermission()
            }
        }

        this.registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted ->
            if (isGranted) {
                //notify to the user
                locationManager.getLocation {
                }
                Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show()
            } else {
                //notify to the user
                Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED  ) {
            Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show()
        }
    }
}



