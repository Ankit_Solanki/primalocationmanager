package com.tikan.primalocationmanager

import android.annotation.SuppressLint
import android.content.Context
import android.location.*
import android.util.Log
import android.widget.Toast
import com.google.android.gms.location.*
import java.util.*

class PrimaLocationManager(private val context: Context) {

    private var latitude: Double = 0.0
    var longitude: Double = 0.0
    lateinit var geocoder: Geocoder
    private lateinit var addresses: List<Address>
    private lateinit var address: String
    private val TAG: String = "PrimaLocationManager"
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    @SuppressLint("MissingPermission")
    fun getLatitude(onComplete: (Double) -> Unit) {
        if (isLocationEnabled()) {
            fusedLocationProviderClient =
                LocationServices.getFusedLocationProviderClient(context)

            fusedLocationProviderClient.lastLocation.addOnCompleteListener { task ->

                val location: Location? = task.result

                if (location == null) {
                    Toast.makeText(context, "something went wrong", Toast.LENGTH_SHORT).show()
                } else {
                    latitude = location.latitude
                    onComplete(latitude)
                }
            }
        } else {
            // if location no enable notify user and show dialog
            GPSUtils(context).turnOnGPS()
        }
    }

    @SuppressLint("MissingPermission")
    fun getLongitude(onComplete: (Double) -> Unit) {
        if (isLocationEnabled()) {
            fusedLocationProviderClient =
                LocationServices.getFusedLocationProviderClient(context)

            fusedLocationProviderClient.lastLocation.addOnCompleteListener { task ->

                val location: Location? = task.result

                if (location == null) {
                    Toast.makeText(context, "something went wrong", Toast.LENGTH_SHORT).show()
                } else {
                    longitude = location.longitude
                    onComplete(longitude)
                }
            }
        } else {
            // if location no enable notify user and show dialog
            GPSUtils(context).turnOnGPS()
        }
    }


    @SuppressLint("MissingPermission")
    fun getLocation(onComplete: (String) -> Unit) {

        if (isLocationEnabled()) {
            fusedLocationProviderClient =
                LocationServices.getFusedLocationProviderClient(context)

            fusedLocationProviderClient.lastLocation.addOnCompleteListener { task ->

                val location: Location? = task.result

                if (location == null) {
                    Toast.makeText(context, "something went wrong", Toast.LENGTH_SHORT).show()
                } else {
                    latitude = location.latitude
                    longitude = location.longitude
                    geocoder = Geocoder(context, Locale.getDefault())
                    addresses = geocoder.getFromLocation(latitude, longitude, 1)
                    address = addresses[0].getAddressLine(0).toString()

                    onComplete(address)

                    Log.d("MyLocation", address)
                }
            }
        } else {
            // if location no enable notify user and show dialog
            GPSUtils(context).turnOnGPS()
        }

    }


    @SuppressLint("MissingPermission")
    fun getCityName(onComplete: (String) -> Unit) {

        if (isLocationEnabled()) {
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context)
            fusedLocationProviderClient.lastLocation.addOnCompleteListener {

                val location: Location? = it.result

                if (location == null) {
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show()
                } else {

                    geocoder = Geocoder(context, Locale.getDefault())
                    addresses = geocoder.getFromLocation(latitude, longitude, 1)
                    val city = addresses.get(0).subAdminArea
                    onComplete(city)
                }
            }
        } else {
            GPSUtils(context).turnOnGPS()
        }
    }

    @SuppressLint("MissingPermission")
    fun getStateName(onComplete: (String) -> Unit) {

        if (isLocationEnabled()) {
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context)
            fusedLocationProviderClient.lastLocation.addOnCompleteListener {

                val location: Location? = it.result

                if (location == null) {
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show()
                } else {
                    geocoder = Geocoder(context, Locale.getDefault())
                    addresses = geocoder.getFromLocation(latitude, longitude, 1)
                    val city = addresses[0].adminArea
                    onComplete(city)
                }
            }
        } else {
            GPSUtils(context).turnOnGPS()
        }

    }

    @SuppressLint("MissingPermission")
    fun getCountryName(onComplete: (String) -> Unit) {

        if (isLocationEnabled()) {
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context)
            fusedLocationProviderClient.lastLocation.addOnCompleteListener {

                val location: Location? = it.result

                if (location == null) {
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show()
                } else {
                    geocoder = Geocoder(context, Locale.getDefault())
                    addresses = geocoder.getFromLocation(latitude, longitude, 1)
                    val country = addresses.get(0).countryName
                    onComplete(country)
                }
            }
        } else {
            GPSUtils(context).turnOnGPS()
        }

    }

    @SuppressLint("MissingPermission")
    fun getCountryCode(onComplete: (String) -> Unit) {

        if (isLocationEnabled()) {
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context)
            fusedLocationProviderClient.lastLocation.addOnCompleteListener {

                val location: Location? = it.result

                if (location == null) {
                    Toast.makeText(context, "Something Went wrong", Toast.LENGTH_SHORT).show()
                } else {
                    geocoder = Geocoder(context, Locale.getDefault())
                    addresses = geocoder.getFromLocation(latitude, longitude, 1)
                    val countryCode = addresses[0].countryCode
                    onComplete(countryCode)
                }
            }
        } else {
            GPSUtils(context).turnOnGPS()
        }
    }

    private fun isLocationEnabled(): Boolean {
        val locationManager: LocationManager =
            context.getSystemService(Context.LOCATION_SERVICE) as LocationManager

        return locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.GPS_PROVIDER
        )
    }

}

