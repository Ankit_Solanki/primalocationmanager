package com.tikan.primalocationmanager

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Layout
import android.widget.TextView
import com.tikan.primalocationmanager.databinding.ActivityShowLocationBinding
import java.util.zip.Inflater

class ShowLocationActivity : AppCompatActivity() {
    lateinit var binding: ActivityShowLocationBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityShowLocationBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.tvLocation.text = intent.getStringExtra("location")
    }
}