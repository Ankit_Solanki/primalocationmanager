package com.tikan.primalocationmanagerlibrary

import android.location.Address
import android.location.Geocoder
import android.widget.TextView
import com.google.android.gms.location.FusedLocationProviderClient

class LocationProvider {

    lateinit var tvLatitude: TextView
    lateinit var tvLongitude: TextView
    lateinit var city: String
    lateinit var address:String
    lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    lateinit var geocoder: Geocoder
    private lateinit var addresses: List<Address>
    private var longitude: Double = 0.0
    private var latitude: Double = 0.0


}